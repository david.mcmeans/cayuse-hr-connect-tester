
settings_file="./conf/settings/settings.sh"
if [ -f "$settings_file" ]; then
  source "$settings_file"
else 
  echo "Settings file $settings_file does not exist. Exiting."
  exit 0;
fi
source ./lib/utils.sh

# force a login (for testing)
# rm "$jwt_file"

# call login script to get JWT 
login_reason=""
if [[ ! -f "$jwt_file" ]]; then
  login_reason="does not exist"
else
  # returns file path if jwt file older than 30 minutes
  jwt_file_isold=$(find $jwt_file -mmin +30)
  if [[ "$jwt_file_isold" ]]; then
    login_reason="is more than 30 min old"
  fi
fi
if [[ "$login_reason" == "" ]]; then
  echo "Login JWT file $jwt_file is less than 30 min old. Proceeding."
else
  echo "Login JWT file $jwt_file $login_reason. Calling login script."
  ./login.sh
fi

# get jwt in calling script
# jwt=$(cat $jwt_file)
