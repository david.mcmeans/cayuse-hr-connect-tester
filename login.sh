
settings_file="./conf/settings/settings.sh"
if [ -f "$settings_file" ]; then
  source "$settings_file"
else
  echo "Settings file $settings_file does not exist. Exiting."
  exit 0;
fi
source ./lib/utils.sh

echo ""
echo "Step 0. LOGIN"
echo ""
echo "Ready to login to $label at $domain."
default="y"
read -e -p "Proceed? [Y/n]:" doLogin
doLogin="${doLogin:-${default}}"
# change to lower case (bash 4.0)
doLogin="${doLogin,,}"
if [[ "$doLogin" == "y" ]]; then
  curl -v "https://signin.$env.cayuse.com/basicauth?tenant_id=$tenantid" -i --basic --user $cayuser:$caypass --write-out %{http_code} --output $dir/login.txt > $dir/login.httpcode.txt 2>"$login_log_file"
  # show verbose login details with credentials masked (recommened)
  cat "$login_log_file" | sed -e 's/Authorization: Basic .*$/Authorization: Basic [base64(user:password)] /' -e 's/Server auth using Basic with user .*/Server auth using Basic with user [user]/' 

  # show verbose login details with credentials included
  # cat "$login_log_file"

  httpResponse=$(cat $dir/login.httpcode.txt)
  if [[ "$httpResponse" == "200" ]] || [[ "$httpResponse" == "202" ]]; then
    # last line of output is JSON web token
    echo ""
    echo "Logged in."
    echo ""
    echo "Login JWT stored in $jwt_file"
    echo ""
    tail -n 1 $dir/login.txt > $jwt_file
  else
    echo "Login returned httpcode $httpResponse"
  fi
fi
