# Usage

## To Upload a CSV File

Start by invoking the upload script

```
$ ./upload.sh
```

Carefully read the screen output to ensure you are connecting to the environment you intend and that you are getting the results you expect. The script
guides you through the upload process.

```
Reading settings from ./conf/settings/settings.sh

Server: Cayuse PROD
Host: myhost.app.cayuse.com
```

If settings.sh is mising, it tells you and exits. (A missing settings.sh file probably means you haven't yet configured the tool. Review the README file for instructions.)

Make sure you see the server and host you expect.

If this is the first time logging in or 30 minutes have passed since your last log in, it prompts you to do so.

```
Login JWT file ./log/jwt.txt does not exist. Calling login script.

Ready to login to Cayuse PROD at myhost.app.cayuse.com.
Proceed? [Y/n]:
```

Press ENTER to accept the default Y.

Curl runs in verbose mode so you can view output from the login. Credentials are masked.

Instead of
```
* Server auth using Basic with user 'your.api.user'
```
you will see
```
* Server auth using Basic with user [user]
```

Instead of
```
> Authorization: Basic eW91ci5hcGkudXNlcjpIanJXRXY3UnNzUVdGX3k=
```
you will see
```
> Authorization: Basic [base64(user:password)]
```

If the login is successful, you will see:

```
* Connection #0 to host signin.app.cayuse.com left intact

Logged in.

Login JWT stored in ./log/jwt.txt
```

Now you are prompted to upload a file or check the status of a prior upload. Since this is our first time, we want to upload.

```
Step 1. UPLOAD or STATUS

Upload a file to Cayuse PROD or check status of prior upload? [U/s]:
```

Press ENTER to accept the default U.

If you do not have any *.csv files in the data folder, it alerts you and exits.

```
No *.csv files found in ./data.
Please place the files you want to upload in ./data and try again.
```

When you do have *.csv files in the ./data folder, a list is shown. Select the one you want by typing its associated number.

```
Select the file to upload:

1) ./data/units.csv
2) Quit
#?
```
Enter 1 and press ENTER to select the first *.csv file.

It attempts to guess at the file's type based on the file header. Select the object type the file contains.

```
The CSV file appears to be of type unit. Select the file's object type:

1) unit         3) extorg       5) user
2) affiliation  4) role         6) Quit
#?
```
Enter 1 and press ENTER.

A final confirmation is shown. Press ENTER to accept the default Y.

```
Ready to upload unit file ./data/units.csv to Cayuse PROD at myhost.app.cayuse.com.
Proceed? (Select n to continue to status step) [Y/n]:
```

Press ENTER to accept the default Y. 

The file is uploaded and the verbose output from curl is shown. As previously mentioned, credentials are masked.

Instead of
```
> Authorization: Bearer eyJraWQiOiJkMDZiNWI1OGMwY...
```
you will see
```
> Authorization: Bearer [jwt]
```

Note the Content Length which shows the size of the file being sent.

```
> Content-Type: text/csv
> X-IDP-New-Login: true
> Content-Length: 34232
```
The end of the upload output shows you the jobID.

```
{ [48 bytes data]
100 34280  100    48  100 34232     30  21475  0:00:01  0:00:01 --:--:-- 21492
* Connection #0 to host myhost.app.cayuse.com left intact
{"jobId":"8294efd7-55ad-430b-a5e9-8a157b7239c7"}
```
Now the status loop begins.

```
Step 3. STATUS

Ready to get status for previous unit job.

Previous jobID is 8294efd7-55ad-430b-a5e9-8a157b7239c7.
Enter jobID [8294efd7-55ad-430b-a5e9-8a157b7239c7]:
```
The script grabs the jobID from the upload output so you do not have to copy and paste. Press ENTER to accept the default jobID and check the status.

If the job is complete, the report is written to ./log/report.txt.

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   133  100   133    0     0    540      0 --:--:-- --:--:-- --:--:--   540
{"totalRowCount":385,"errors":0,"successes":385,"completed":true,"status":"Completed","jobId":"8294efd7-55ad-430b-a5e9-8a157b7239c7"}

Job status is: Complete.
```

When the job is still processing, you will see output like this:

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   131  100   131    0     0    671      0 --:--:-- --:--:-- --:--:--   671
{"totalRowCount":0,"errors":0,"successes":0,"completed":false,"status":"Processing","jobId":"60a7bdc0-43d9-4b03-9604-86d83930e4be"}

Job status is: Pending.

Check status again? [Y/n/<seconds to wait>]:
```
The script prompts you to check again, until finally:

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   138  100   138    0     0    690      0 --:--:-- --:--:-- --:--:--   690
{"totalRowCount":2364,"errors":1025,"successes":1339,"completed":true,"status":"Completed","jobId":"60a7bdc0-43d9-4b03-9604-86d83930e4be"}

Job status is: Complete.

https://myhost.app.cayuse.com/api/v2/administration/batch/report/unit?jobId=8294efd7-55ad-430b-a5e9-8a157b7239c7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 60433  100 60433    0     0   179k      0 --:--:-- --:--:-- --:--:--  179k

Output written to ./log/report.txt
```
You may review the output from the upload by editing ./log/report.txt.

## To Check Status of a Prior Upload

To check status of a prior job, you need to know the 

- the file object type
- the jobID

The jobID goes in ./log/jobID.txt and the file object type goes in ./log/obj.txt.

Then invoke the upload script

```
$ ./upload.sh
```

When you are prompted to upload a file or check the status of a prior upload, select **s** and press ENTER.

```
Step 1. UPLOAD or STATUS

Upload a file to Cayuse PROD or check status of prior upload? [U/s]:
```

The "ready to get status" notice appears. Verify that the object type matches what you expect. Here, the object type is *affiliation*.

```
Step 3. STATUS

Ready to get status for previous affiliation job.

Previous jobID is 0ce083af-ecfb-471c-878d-bc07ce6e1ce0.
Enter jobID [0ce083af-ecfb-471c-878d-bc07ce6e1ce0]:
```

If the object type or the jobID are not correct, exit the script and make corrections in ./log/obj.txt and ./log/jobid.txt.

Once they are correct, continue by pressing ENTER.

The status is shown. In the example below, the job is complete so the report is retrieved as well.

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    88  100    88    0     0     32      0  0:00:02  0:00:02 --:--:--    32
{"rowsCompleted":1862,"errors":6,"successes":1856,"totalRowCount":1862,"completed":true}

Job status is: Complete.

https://myhost.app.cayuse.com/api/v2/administration/batch/report/affiliation?jobId=0ce083af-ecfb-471c-878d-bc07ce6e1ce0
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    41  100    41    0     0      1      0  0:00:41  0:00:29  0:00:12    10

Output written to ./log/report.txt

It took 34.237 seconds to complete the upload (give or take)
```

## Automating the Status Check

If the status check is taking more than a few passes, you may enter a number of seconds to automatically wait between checks. The minimum is 10 seconds.

```
100   131  100   131    0     0    740      0 --:--:-- --:--:-- --:--:--   740
{"totalRowCount":0,"errors":0,"successes":0,"completed":false,"status":"Processing","jobId":"427df51d-047d-4985-a4b9-e42720f6cb75"}

Job status is: Pending.

Check status again? [Y/n/<seconds to wait>]:
```
Enter 15 and press ENTER.

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   131  100   131    0     0    789      0 --:--:-- --:--:-- --:--:--   789
{"totalRowCount":0,"errors":0,"successes":0,"completed":false,"status":"Processing","jobId":"6adfbbc3-8c0c-41c4-a619-574bb556fcb7"}

Job status is: Pending.

Waiting 15 seconds.
```
The status check continues every 15 seconds until the job is done. Time to grab that coffee!
