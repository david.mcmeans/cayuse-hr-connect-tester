# Changes

This is a list of changes based on errors that have been returned by the API.

### July 2023; spaces no longer permitted around column names or after the commas in header

{"errors":[{"message":"Headers received do not match expected headers. See documentation for required headers and options headers.","expectedHeaders":"Expected headers are: [First Name, Middle Name, Last Name, Prefix, Suffix, Preferred Name, Active, Employee ID, Email, Username, NIH Commons ID, NSF ID, NASA NSPIRES ID, ORCID ID, Degree Description, Degree Year, Degree Qualifications, Gender, Ethnicity, Races, Disabilities, Citizenship, Home Phone, Fax, Street 1, Street 2, Country, State/Province, County, City, Postal Code, Emergency Contact, Office Phone, Pager, Preferred Contact Method, Mobile Phone, Create Account, User Active]","receivedHeaders":"Received Headers are: [First Name, Middle Name, Last Name, Prefix, Suffix, Preferred Name,  Active,  Employee ID,  Email,  Username,  Gender,  Create Account,  User Active]"}]}

#### Solution

The CSV headers must not have spaces around the column names

### cannot duplicate people in user upload

Zhiqiang,,Wu,Dr.,,,true,U0034XXXXX,zhiqiang.wu@wright.edu,w001XXX,MALE,true,true,,Record import failed.,Employee ID must be unique within CSV file; two or more CSV entries are claiming this ID.,

#### Solution

Pre-filter the personnel before exporting to CSV. Ideally, use a single select.
