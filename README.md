
# Name
Cayuse HR Connect tester

## Description
A set of bash scripts for testing Cayuse HR Connect. The Cayuse HR Connect API supports bulk updates via CSV uploads. 

[Cayuse](https://cayuse.com/) is a software platform that provides research administration solutions for academic institutions, research organizations, and research-focused businesses. It helps reduce administrative burden, increase efficiency, and improve compliance and oversight. 

The [Cayuse Support](https://support.cayuse.com) website describes how to access and test the HR Connect API to make bulk updates. These scripts are based on those instructions.

An organization will typically use a tool like ODI or JitterBit to manage API communications. These scripts are for quick testing and development and could be used to verify solutions eventually rolled into those tools. 

## Installation
Within a linux ecosystem, install by cloning the project.

```bash
$ mkdir cayuse-tester
$ cd cayuse-tester
$ git clone https://gitlab.com/david.mcmeans/cayuse-hr-connect-tester.git .
```

Copy the default settings and make changes.

```bash
$ cd conf
$ cp default.settings.conf settings/settings.conf
$ vi settings/settings.conf
```

 At the top, uncomment the environment you wish to use. Set the values marked *Set this*.

```bash
# uncomment the server you want to use
#server="test"
server="prod"

cayuse_uat_tenantid="example-2255-49d9-a210-9751769c563a" # Set this
cayuse_app_tenantid="example-53c2-4789-ba56-0cb172af3cb0" # Set this

if [[ "$server" == "prod" ]]; then
  label="Cayuse PROD"
  host="hostprod" # Set this - If your production domain is spirit.app.cayuse.com, then set this to spirit
  env="app"
  cayuser="api.user" # Set this
  caypass="password" # Set this
  tenantid="$cayuse_app_tenantid"
else
  label="Cayuse UAT"
  host="hostuat" # Set this - if you test domain is spirit-t.app.cayuse.com, then set this to spirit-t
  env="uat"
  cayuser="api.user" # Set this
  caypass="password" # Set this
  tenantid="$cayuse_uat_tenantid"
fi
```

### Requirements
The scripts use jq for processing json data and a number of commands like sed, tr, find, head, and curl which are assumed to be already available. Download and installation instructions for jq are [here](https://stedolan.github.io/jq/download/).

# Install jq for GitBash on Windows
Run gitbash as Administrator
curl -L -o /usr/bin/jq.exe https://github.com/stedolan/jq/releases/latest/download/jq-win64.exe


The account whose credentials you enter in ./conf/settings/settings.conf, must be provisioned to use the HR Connect API. For example, you might
need to grant the account the **HR Connect Administrator** role. See to the Cayuse Support site for the latest information.

## Usage

### To Upload a CSV File

TLDR;

Place the *.csv files you want to upload in the ./data folder.

Run the upload script

```
$ ./upload.sh
```

Carefully read the screen output to ensure you are connecting to the environment you intend and that you are getting the results you expect. The script
guides you through the upload process.

[Detailed Upload Instructions](UPLOAD.md)


## Support

For assistance, you can reach me at david.mcmeans@wright.edu.

## Roadmap

- Add more object types for uploads.
- Add basic support for retrieving content from the v1 and v2 API.

## Contributing

Suggestions and code contributions welcome!

## Authors and acknowledgment

David S. McMeans

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Project status

Actively developed.
