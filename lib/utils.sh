
# include after conf/settings/settings.sh

# work directory
dir="./log"

# files
jwt_file="$dir/jwt.txt"
jobid_file="$dir/jobid.txt"
obj_file="$dir/obj.txt"
upload_file="$dir/upload.txt"
upload_log_file="$dir/upload.log"
status_file="$dir/status.txt"
report_file="$dir/report.txt"
report_errors_file="$dir/report_errors.txt"
login_log_file="$dir/login.log"
firstline_file="$dir/firstline.txt"
cleanline_file="$dir/cleanline.txt"
getobjwarn_file="$dir/getobjwarn_file.txt"

# v1
v1domain="developer.$env.cayuse.com"
v1pathuri="v1"

#v2 (HR Connect and app use this)
v2domain="$host.$env.cayuse.com"
v2pathuri="api/v2"

path="$v2pathuri/administration/batch"
domain="$v2domain"

obj_list=("unit" "affiliation" "extorg" "role" "user")
