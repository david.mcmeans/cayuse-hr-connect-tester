# requires curl and jq
# presume curl is already installed in linux ecosystem

v=(`jq --version`)
if [[ $v == "" ]]; then
  echo ""
  echo "Checking requirements:"
  echo ""
  echo "jq appears not to be installed. Please install jq before proceeding."
  echo ""
  echo "For gitbash in Windows:"
  echo "  Run gitbash as Administrator"
  echo "  In gitbash, run:"
  echo "  curl -L -o /usr/bin/jq.exe https://github.com/stedolan/jq/releases/latest/download/jq-win64.exe"
  echo ""
  echo "For other environments, see https://jqlang.github.io/jq/download/"
  exit 0
fi
