
settings_file="./conf/settings/settings.sh"
if [ -f "$settings_file" ]; then
  source "$settings_file"
else 
  echo "Settings file $settings_file does not exist. Exiting."
  exit 0;
fi
source ./lib/utils.sh

echo ""
echo "UPLOAD"
echo ""
source ./preamble.sh
source ./lib/checkreqs.sh

./check-login.sh

jwt=$(cat $jwt_file)

echo ""
echo "Step 1. UPLOAD or STATUS"
echo ""
read -e -p "Upload a file to $label or check status of prior upload? [U/s]:" doChoose
default="u"
doChoose="${doChoose:-${default}}"
# change to lower case (bash 4.0)
doChoose="${doChoose,,}"

if [[ "$doChoose" == "u" ]]; then
 
  # build list of csv files in data folder
  unset options i
  while IFS= read -r -d $'\0' f; do
    options[i++]="$f"
  done < <(find ./data/ -maxdepth 1 -type f -name "*.csv" -print0 )

  # if no csv files, quit
  if [[ "${#options[@]}" == 0 ]]; then
    echo ""
    echo "No *.csv files found in ./data."
    echo "Please place the files you want to upload in ./data and try again."
    echo ""
    exit
  fi

  echo "Select the file to upload:"
  echo ""
  select opt in "${options[@]}" "Quit"; do
    case $opt in
      *.csv)
        echo "CSV file $opt selected"
	csv_file="$opt"
        break
        ;;
      Quit)
        echo "You chose to stop"
	exit 0
        ;;
      *)
        echo "This is not a number"
        ;;
    esac
  done

  obj_type=$(./get-objtype.sh $csv_file)

  # show get-objtype warnings if any
  cat "$getobjwarn_file"
  
  unset options i
  options=("${obj_list[@]}")
  echo ""
  echo "The CSV file appears to be of type $obj_type. Select the obj type it contains:"
  echo ""
  select opt in "${options[@]}" "Quit"; do
    case $opt in
      unit | affiliation | extorg | role | user)
        echo "Data type $opt selected"
        obj="$opt"
        if [[ "$obj" == "user" ]]; then
           parm="?send_account_activation_emails=false"
        fi
        break
        ;;
      "Quit")
        echo "You chose to stop"
        exit 0
        ;;
      *)
        echo "This is not a number"
        ;;
    esac
  done
  # save obj type for status request later
  echo "$obj" > "$obj_file"

  echo ""
  echo "Step 2. UPLOAD"
  echo ""
  echo "Ready to upload $obj file $csv_file to $label at $domain."
  default="y"
  read -e -p "Proceed? (Select n to continue to status step) [Y/n]:" doUpload
  doUpload="${doUpload:-${default}}"
  # change to lower case (bash 4.0)
  doUpload="${doUpload,,}"
  
  if [[ "$doUpload" == "y" ]]; then
  
    # check file exists
    if [ -f "$csv_file" ]; then
      echo "$csv_file exists."
    else 
      echo "$csv_file does not exist. Exiting."
      exit 0;
    fi
  
    # Call to upload the CSV
    # jobID is written to upload file
    echo "https://$domain/$path/upload/$obj$parm"
    curl -v -H "Authorization: Bearer $jwt" -H "Content-Type: text/csv" -H "X-IDP-New-Login: true" -X POST --data-binary "@$csv_file" "https://$domain/$path/upload/$obj$parm" --output "$upload_file" 2>"$upload_log_file"

    # show upload log details with credentials masked (recommended)
    cat "$upload_log_file" | sed -e 's/Authorization: Bearer .*/Authorization: Bearer [jwt]/'

    # show upload log details including credentials 
    #cat "$upload_log_file" 

    cat "$upload_file"
     jq -r ".jobId" "$upload_file" > "$jobid_file"
  
    # expect {"jobId":"66c586ae-31cb-47a9-9155-bf76460a3bec"}
  fi
fi

doStatus="TRUE"
if [[ "$doStatus" == "TRUE" ]]; then

  TIMEFORMAT='It took %R seconds to complete the upload (give or take)'

  time {

    #get obj type if we are requesting status later
    obj=$(cat "$obj_file")

    echo ""
    echo ""
    echo "Step 3. STATUS"
    echo ""
    echo "Ready to get status for previous $obj job."
    echo ""
    default=$(cat "$jobid_file") 
    echo "Previous jobID is $default."
    read -e -p "Enter jobID [$default]:" jobid
    jobid="${jobid:-${default}}"
    echo " "
    echo "$jobid" > "$jobid_file"
    echo ""

    # Get job status
    doWait="y"
    isComplete="false"
    status="Pending"
    autoWait="0"
  
    while [[ $isComplete == "false" ]] && [[ $doWait == "y" ]]
    do
      curl -H "Authorization: Bearer $jwt" -H "X-Idp-New-Login: true" "https://$domain/$path/status/$obj?jobId=$jobid" --output "$status_file"
      cat "$status_file"
      #isComplete=$(jq -r ".completed" "$status_file")
      read isComplete < <(echo $(jq -r '.completed' "$status_file"))
      status="Pending"
      if [[ $isComplete == "true" ]]; then
        status="Complete"
      fi
      echo ""
      echo ""
      echo "Job status is: $status."
      echo ""
      echo ""
      if [[ $isComplete == "false" ]]; then
        if [[ "$autoWait" -gt "0" ]]; then
          echo ""
          echo "Waiting $autoWait seconds."
          echo ""
          sleep "$autoWait"
	else
          read -e -p "Check status again? [Y/n/<seconds to wait>]:" doWait
          default="y"
          doWait="${doWait:-${default}}"
          doWait="${doWait,,}"
	  re='^[0-9]+$'
	  if [[ $doWait =~ $re ]] ; then
	    autoWait="$doWait"
	    if [[ "$autoWait" -lt 10 ]]; then
	      autoWait=10
            fi
	    doWait="y"
          fi
        fi
      fi

    done
  
    # Get job report once status is Completed
    if [[ "$isComplete" == "true" ]]; then
      echo "https://$domain/$path/report/$obj?jobId=$jobid"
    #curl -H "Authorization: Bearer $jwt" -H "X-Idp-New-Login: true" "https://$domain/$path/report/$obj?jobId=$jobid"
      curl -H "Authorization: Bearer $jwt" -H "X-Idp-New-Login: true" "https://$domain/$path/report/$obj?jobId=$jobid" 1> "$report_file"
  
      echo ""
      echo "Output written to $report_file"
      echo ""

      # show any failures
      grep fail "$report_file" > "$report_errors_file"
      if [[ -s "$report_errors_file" ]]; then
        less "$report_errors_file"
      fi

    fi
  }
fi

echo " "

