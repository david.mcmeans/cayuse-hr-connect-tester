
settings_file="./conf/settings/settings.sh"
if [ -f "$settings_file" ]; then
  source "$settings_file"
else
  echo "Settings file $settings_file does not exist. Exiting."
  exit 0;
fi
source ./lib/utils.sh

#
# upload csv file with incorrect heaers, API will respond with expected headers for objType
#
declare -a headers=(
  ""
  "Username,Role,Unit Primary Code,Include All Sub Units"
  "First Name,Middle Name,Last Name,Prefix,Suffix,Preferred Name,Active,Employee ID,Email,Username,Gender,Create Account,User Active"
  "Username,Employee ID,Unit Primary Code,Unit Name,Title,Primary Appointment,US Government Agency,Contact Email,Contact Office Phone,Contact Mobile Phone,Contact Pager,Contact Fax,Contact Preferred Contact Method,Contact Street 1,Contact Street 2,Contact County,Contact Country,Contact State/Province,Contact City,Contact Postal Code,Contact Website,Performance Site Organization,Performance Site Active,Performance Site DUNS,Performance Site Email,Performance Site Office Phone,Performance Site Mobile Phone,Performance Site Pager,Performance Site Fax,Performance Site Preferred Contact Method,Performance Site Street 1,Performance Site Street 2,Performance Site County,Performance Site Country,Performance Site State/Province,Performance Site City,Performance Site Postal Code,Performance Site Website,Performance Site Congressional District,Calendar Months,Calendar Salary,Academic Months,Academic Salary,Summer Months,Summer Salary,Principal Investigator,Assistant,Administration Official,Signing Official,Payee"
  "Name,Short Name,Primary Code,Secondary Code,Parent Unit Primary Code,Active"
)

declare -a objs=(
  "header missing?"
  "role"
  "user"
  "affiliation"
  "unit"
)

file="$1"

# Jul 2023 warn about spaces after commas which are no longer permitted and cause the upload to fail

temp_file="$(mktemp)"
tr '\r' '\n' < "$file" > "$temp_file"

# get first line of csv, which should be the header
head -1 "$temp_file" > "$firstline_file"

# remove double quotes
sed -i -e's/"//g' "$firstline_file"

# remove spaces after commas
sed -e's/ *, */,/g' "$firstline_file" > "$cleanline_file"

# clear warnings file
cp /dev/null "$getobjwarn_file"

files_match="$(cmp --silent "$firstline_file" "$cleanline_file"; echo $?)"
if [[ "$files_match" -ne 0 ]]; then
  echo " " > "$getobjwarn_file"
  echo "*************************************" >> "$getobjwarn_file"
  echo "WARNING: " >> "$getobjwarn_file"
  echo " " >> "$getobjwarn_file"
  echo "The header in your selected CSV file" >> "$getobjwarn_file"
  echo "may contain spaces around the column names." >> "$getobjwarn_file"
  echo "This may cause the upload to fail." >> "$getobjwarn_file"
  echo "Please make sure the header looks like:" >> "$getobjwarn_file"
  echo "First Name,Middle Name,Last Name,Email,..." >> "$getobjwarn_file"
  echo " " >> "$getobjwarn_file"
  echo "Header is: " >> "$getobjwarn_file"
  cat "$firstline_file" >> "$getobjwarn_file"
  echo " " >> "$getobjwarn_file"
  echo "*************************************" >> "$getobjwarn_file"
  echo " " >> "$getobjwarn_file"
fi

line=$(<"$cleanline_file")

# Search the array for the first match and store the index in a variable
match=0
for i in "${!headers[@]}"; do
  if [[ "${headers[i]}" == "$line" ]]; then
    match="$i"
    break
  fi
done

echo "${objs[$match]}"

