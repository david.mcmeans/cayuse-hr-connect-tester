
# uncomment the server you want to use
#server="test"
server="prod"

cayuse_uat_tenantid="example-2255-49d9-a210-9751769c563a" # Set this
cayuse_app_tenantid="example-53c2-4789-ba56-0cb172af3cb0" # Set this

if [[ "$server" == "prod" ]]; then
  label="Cayuse PROD"
  host="hostprod" # Set this - If your production domain is spirit.app.cayuse.com, then set this to spirit
  env="app"
  cayuser="api.user" # Set this
  caypass="password" # Set this
  tenantid="$cayuse_app_tenantid"
else
  label="Cayuse UAT"
  host="hostuat" # Set this - if you test domain is spirit-t.app.cayuse.com, then set this to spirit-t
  env="uat"
  cayuser="api.user" # Set this
  caypass="password" # Set this
  tenantid="$cayuse_uat_tenantid"
fi
